from django.urls import path
from . import views

app_name = 'crud'

urlpatterns = [
    #calling a class based view
    path('', views.IndexView.as_view(), name="crudhomepage"),
    path('add/', views.AddView.as_view(), name='add'),
    path('posts/', views.PostsView.as_view(), name='posts'),
    path('<slug:slug>/', views.SingleView.as_view(), name='single'),
    path('edit/<int:pk>/', views.EditView.as_view(), name='edit'),
    path('delete/<int:pk>/', views.DeleteView.as_view(), name='delete'),
]
