from django.shortcuts import render
from .models import Core
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


# generic views imported above, collects all info in the db
class IndexView(ListView):
    # this var names the db model
    model = Core
    # pass object to the crud index file
    template_name = 'crudindex.html'
    # this contains the db with the name crudindex and is ready to be passed in django templating
    context_object_name = 'crudindex'


class SingleView(DetailView):
    model = Core
    template_name = 'single.html'
    context_object_name = 'post'


class PostsView(ListView):
    model = Core
    template_name = 'crudposts.html'
    context_object_name = 'post_list'


class AddView(CreateView):
    model = Core
    template_name = 'add.html'
    fields = '__all__'
    success_url = reverse_lazy('crud:posts')


class EditView(UpdateView):
    model = Core
    template = 'edit.html'
    fields = '__all__'
    pk_url_kwarg = 'pk'
    success_url = reverse_lazy('crud:posts')

class DeleteView(DeleteView):
    model = Core
    pk_url_kwarg = 'pk'
    success_url = reverse_lazy('crud:posts')
    template_name = 'confirm-delete.html'