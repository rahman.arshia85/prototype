from django.contrib import admin
from .models import Core

# Register your models here.
@admin.register(Core)
class PostAdmin(admin.ModelAdmin):
    #Found in django docs used for syncing the title and slug fields
    prepopulated_fields = {'slug': ('title',), }