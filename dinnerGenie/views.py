# Create your views here.
import json

from django.shortcuts import render
from .forms import SubmitForm
import requests
import random


def intropage(request):
    context = {}
    return render(request, "intro.html", context)


# Create your views here.
def dgpage(request):
    # if this is a POST request we need to process the data within the form
    if request.method == 'POST':
        form = SubmitForm(request.POST)
        # check whether it's valid
        if form.is_valid():
            # declare form variables
            zipcode = form.cleaned_data['zipcode']
            distance = form.cleaned_data['distance']
            price = form.cleaned_data['price']
            GOOGLE_MAPS_API_URL = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'

            # convert int to string with f-string
            c_zipcode = f'{zipcode}'
            c_distance = f'{distance}'
            c_price = f'{price}'
            req_query = 'restaurants+' + c_zipcode

            params = {
                'key': 'AIzaSyBo5PrV2WYdqspwPPLJRVaRHspx1k8kSY4',
                'query': req_query,
                'maxprice': c_price,
                'radius': c_distance
            }
            req = requests.post(GOOGLE_MAPS_API_URL, params=params)
            res = req.json()

            q_result = []
            for data in res['results']:
                q_result.append(data['name'])

            # Select random place
            genie_choice_name = random.choice(q_result)

            return render(request, "response.html", {'form_py': form, 'names': q_result,
                                                     'genie_choice_name': genie_choice_name})
    else:
        form = SubmitForm
    s_result = ''
    return render(request, "dg.html", {'form_py': form, 'names': s_result})
