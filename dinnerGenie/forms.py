from django import forms


class SubmitForm(forms.Form):
    zipcode = forms.IntegerField()
    distance = forms.ChoiceField(choices=[(10000, '5'), (20000, '10'), (30000, '15'), (40000, '20'), (50000, '25 +')])
    price = forms.ChoiceField(choices=[(1, '$'), (2, '$$'), (3, '$$$'), (4, '$$$$')])
