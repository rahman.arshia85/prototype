from django.apps import AppConfig


class DinnergenieConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dinnerGenie'
