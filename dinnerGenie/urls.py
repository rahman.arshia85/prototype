from django.urls import path
from . import views

urlpatterns = [
    path('', views.dgpage, name='dgpage')
]
