"""Prototype URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from register import views as v
from dinnerGenie import views as p


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('landingpage.urls')),
    path('dinnergenie/', include('dinnerGenie.urls')),
    path('crud/', include('crud.urls', namespace='crud')),
    path('tictactoe/', include('tictactoe.urls')),
    path("register/", v.register, name="register"),
    path("login/", v.loginpage, name="loginpage"),
    path("logout/", v.logoutfunc, name="logout"),
    path("secret/", v.secret, name="secret"),
    path("intro/", p.intropage, name="intro"),
]
