using Microsoft.AspNetCore.Mvc;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

[ApiController]
[Route("api/[controller]")]
public class FileUploadController : ControllerBase
{
    [HttpPost("upload")]
    public async Task<IActionResult> UploadCsv([FromForm] IFormFile file)
    {
        if (file == null || file.Length == 0)
        {
            return BadRequest("No file was uploaded or the file is empty.");
        }

        using var stream = file.OpenReadStream();
        using var reader = new StreamReader(stream);

        // If there's a header row you want to skip:
        // var header = await reader.ReadLineAsync();

        int lineNumber = 0;
        while (!reader.EndOfStream)
        {
            lineNumber++;
            var line = await reader.ReadLineAsync();
            if (string.IsNullOrWhiteSpace(line))
                continue; // skip empty lines

            var columns = line.Split(',');

            // Validate each column in this row
            for (int i = 0; i < columns.Length; i++)
            {
                string columnValue = columns[i].Trim();

                // 1) Check integer
                if (int.TryParse(columnValue, out _))
                {
                    continue;
                }

                // 2) Check decimal (123.45)
                if (double.TryParse(columnValue, NumberStyles.Any, CultureInfo.InvariantCulture, out _))
                {
                    continue;
                }

                // 3) Check date (yyyy-MM-dd)
                if (IsValidDateString(columnValue))
                {
                    continue;
                }

                // 4) Check alphabetic strings
                if (IsAlpha(columnValue))
                {
                    continue;
                }

                // If none of the checks passed, return an error
                return BadRequest(
                    $"Invalid data at line {lineNumber}, column {i + 1}: '{columnValue}'"
                );
            }
        }

        return Ok("CSV validated successfully. Accepted types: integer, decimal, date (yyyy-MM-dd), or alphabetic strings.");
    }

    private bool IsAlpha(string input)
    {
        // Returns true if every character is a letter (A-Z, a-z).
        // If you want to allow spaces, punctuation, or alphanumeric, modify this logic.
        return input.All(char.IsLetter);
    }

    private bool IsValidDateString(string input)
    {
        // Strict check for yyyy-MM-dd format
        return System.DateTime.TryParseExact(
            input,
            "yyyy-MM-dd",
            CultureInfo.InvariantCulture,
            DateTimeStyles.None,
            out _
        );
    }
}
