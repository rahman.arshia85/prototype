import { Component, OnInit } from '@angular/core';
import { BoostService } from '../boost.service';

@Component({
  selector: 'app-display',
  template: `
    <div *ngIf="message">
      {{ message }}
    </div>
  `,
})
export class DisplayComponent implements OnInit {
  message: string = '';

  constructor(private apiService: BoostService) {}

  ngOnInit(): void {
    this.apiService.getMessage().subscribe({
      next: (data) => {
        this.message = data.message;
      },
      error: (err) => console.error(err)
    });
  }
}
