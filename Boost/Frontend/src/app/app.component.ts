import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { BoostService } from './boost.service';


@Component({
  selector: 'app-root',
  // If using standalone components, be sure to set standalone: true
  // and import your RouterOutlet here:
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Frontend';

  constructor(private boostService: BoostService) {}

  ngOnInit(): void {
    // Make the call to your .NET backend on init
    this.boostService.getMessage().subscribe({
      next: (res) => {
        console.log('Response from .NET API:', res);
      },
      error: (err) => {
        console.error('Error calling .NET API:', err);
      }
    });
  }
}
