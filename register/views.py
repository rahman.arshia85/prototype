from django.shortcuts import render, redirect
from .forms import RegisterForm
from django.contrib.auth import authenticate, login, logout


# Create your views here.
def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/login/')
    else:
        form = RegisterForm()
    context = {"form": form}
    return render(request, "registration.html", context)


def loginpage(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect("/dinnergenie/")

        else:
            return redirect("/login/")

    context = {}
    return render(request, "login.html", context)


def logoutfunc(request):
    logout(request)
    return redirect("/login/")


def secret(request):
    return render(request, "secret.html", {})
