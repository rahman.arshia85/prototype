from django.shortcuts import render
from .forms import ContactForm


# Create your views here.
def landingpage(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'success.html')
    # Updated form from html to using django forms
    form = ContactForm()
    context = {'form': form}
    return render(request, "index.html", context)
